# scrape conference talk text from lds.org
import urllib.request
from LdsorgMainHtmlParser import LdsorgMainHtmlParser
#from LdsorgConfHtmlParser import LdsorgConfHtmlParser
#from LdsorgTalkHtmlParser import LdsorgTalkHtmlParser


def get_page_data(url, parser):
    page = urllib.request.urlopen(url)
    src = str(page.read())
    print(src)
    parser.feed(src)
    return parser.data

main_parser = LdsorgMainHtmlParser()
main_url = 'https://www.lds.org/general-conference/conferences'
main_url = 'http://www.lds.org'
conf_urls = get_page_data(main_url, main_parser)
print(conf_urls)
quit()

# iterate through lds.org conference talks
for conf_url in conf_urls:
    # make directory in file system for conf
    conf_parser = LdsorgConfHtmlParser()
    talk_urls = get_page_data('http://lds.org' + conf_url, conf_parser)
    for talk_url in talk_urls:
        talk_parser = LdsorgTalkHtmlParser()
        talk_txt = get_page_data('http://lds.org' + talk_url, talk_parser)
        # save talk text to a file
        ls = talk_url.split('/')
        year = ls[1]
        month = ls[2]
        title = ls[3][:-9]
        f = open(year + '/' + month + '/' + title, 'w')
        f.write(talk_text)
        f.close()
