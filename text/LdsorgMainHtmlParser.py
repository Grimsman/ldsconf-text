# parses the conference urls from lds.org
from html.parser import HTMLParser


class LdsorgMainHtmlParser(HTMLParser):

    def __init__(self):
        HTMLParser.__init__(self)
        self.data = []

    def handle_starttag(self, tag, attrs):
        print('found start tag: ' + tag)
        print('attrs: ' + str(attrs))
        if tag == 'a':
            print('found link')
            save = False
            for attr in attrs:
                if attr[0] == 'class' and attr[1] == 'year-line__link':
                    save = True
                if attr[0] == 'href':
                    link = attr[1]
            if save:
                self.data.append(link)
